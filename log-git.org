backup

* Intención
 * Aprender a usar re-frame
 * Usar componentes básicos de Material-UI
   
* Logros en este commit
  1) corregidas erratas.
   
* Logros acumulados en rama
  1) corregidas erratas.
     
* Otros y spin-off 
  * Letras raras en emacs \ #*+_-/&%$"¡!¿?ñÑ{}'() @ []]] #_

* Por hacer
  En general (no por orden de importancia)
  1) mejora del UI de login/out 
 
* CÓMO
  ejecutar      npx shadow-cljs watch frontend
    o bien      npm run watch
  guardar con   git add -A ; git commit -F log-git.org
  Firestore     https://console.firebase.google.com/project/... 
  Firestire docu https://firebase.google.com/docs/firestore     
  Plat.enCloud  https://xxxx.web.app
  firebase deploy ;; para enviar al servidor de google 

* EL PRIMER ARRANQUE
  instalar con: git clone https://gitlab.com/meetsCode/demo-re-frame.git
  ejecutar:
     $ npm install
     $ npm install -s @mui/material @emotion/react @emotion/styled
     $ npm install -s @mui/icons-material
     $ npm install -g firebase firebase-tools
