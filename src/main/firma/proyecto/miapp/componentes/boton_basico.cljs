(ns evalucar.evalufall.miapp.componentes.boton-basico
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ["@material-ui/core" :refer [Box Button]]
            [evalucar.evalufall.miapp.eventos-efectos.ver-basicos ]
            [evalucar.evalufall.miapp.firebase.firestore :as fs   ]
            ;;#_#_#_#_# Ejemplo #####@@@@|||\\\
            ))


(defn ver-db-button
  []
  [:> Button  {:onClick (fn [evt] (rf/dispatch [:ver-db nil]) )} "ver db"])


(defn ver-cntxt-button
  []
  [:button  {:on-click (fn [evt]
                         (.log js/console evt)
                         (rf/dispatch [:ver-cntxt nil]) )} "ver cntxt"])

(defn ver-libre-button
  []
  [:div
   #_[:h1 "Bienvenido a la aplicación de cálculo de impactos"]
   #_(let [usuario (rf/subscribe [:query-usr-uid])]
       (if usuario
         [:button.btn.btn--link.float--right
          { :onClick #(auth/sign-out-google)}
          "logout"]
         [:button.btn.btn--link.float--right
          { :onClick #(auth/sign-in-with-google)}
          "login"]))
   [:p (str "Texto que envío:" )]
   [:input {:onChange (fn [evnt]
                        ;;(.log js/console "evnt")
                        ;;(.log js/console evnt)
                        (println (-> evnt .-target .-value))
                        ;;(rf/dispatch [:guarda-usr (-> evnt .-target .-value)])
                        (fs/inicializo-firestore)
                        )}]
   [:button {:onClick (fn [evnt]

                        (println (-> evnt .-target .-value))
                        (fs/inicializo-firestore  )
                        ;;(rf/dispatch [:contexto-y-db ])
                        ;; creo la instancia a la base de datos.
                        )}"botón"]])
