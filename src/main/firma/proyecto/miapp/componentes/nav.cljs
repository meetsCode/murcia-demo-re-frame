(ns evalucar.evalufall.miapp.componentes.nav 
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ;;##### Ejemplo #####@@@@|||\\\
            [evalucar.evalufall.miapp.eventos-efectos.usuario]
            [evalucar.evalufall.miapp.querys.usuario]
            [reagent-material-ui.core.grid :refer [grid]]
            ["@material-ui/core" :refer [Box Button]]
            [evalucar.evalufall.miapp.componentes.log-in-out :refer [log-in-button log-out-button]]
            [evalucar.evalufall.miapp.componentes.boton-basico :as basico]
            ))


(def menu-logeado [{:key "Segundo" 
                    :id :segundo
                    :xs 2
                    :component "a"
                    :href "#segundo"
                    :ml 2
                    :pb 2}
                   {:key "Log-out"
                    :id :log-out
                    :xs 2
                    :component "a"
                    :href "#log-out"
                    :ml 2
                    :pb 2}])


(defn nav []
  (let [usuario @(rf/subscribe [:query-usr-uid] )]
    [grid {:container true
           :item true
           :spacing 3
           :justify "space-between"}
     [grid {:item true} "Icono CocheChoca"]
     (if usuario 
       [grid {:item true} [log-out-button]]
       [grid {:item true} [log-in-button]]
       )
     ]))


(defn nav-extra-dev []
    [grid {:container true :item true :spacing 2} 
     [grid {:item true} [basico/ver-db-button]]
     [grid {:item true} [basico/ver-cntxt-button]]
     [grid {:item true} [basico/ver-libre-button]]]
 )
