(ns evalucar.evalufall.miapp.componentes.log-in-out
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ["@material-ui/core" :refer [Box Button]]
            ;;##### Ejemplo #####@@@@|||\\\
            [evalucar.evalufall.miapp.eventos-efectos.ev-log-in-out]
            ))


(defn log-in-button
  []
  [:> Button  {:onClick (fn [evt] (rf/dispatch [:log-in-popup-google nil]) )} "log-in"])


(defn log-out-button
  []
  [:> Button  {:onClick (fn [evt] (rf/dispatch [:log-out nil]) )} "log-out"])
