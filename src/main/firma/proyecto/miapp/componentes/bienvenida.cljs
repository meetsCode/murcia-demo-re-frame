(ns evalucar.evalufall.miapp.componentes.bienvenida
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ;;##### Ejemplo #####@@@@|||\\\
            ))

(defn bienvenida []
  [:div
	 [:hr]
	 [:p (str "La fecha/hora actual es: " )]
	 [:h3 (str (.toISOString (js/Date.)))]
   [:header
    [:h1 "Bienvenido a la aplicación de cálculo de impactos"]
    ]])
