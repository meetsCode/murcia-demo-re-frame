(ns evalucar.evalufall.miapp.componentes.cuerpo-web
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ;;##### Ejemplo #####@@@@|||\\\
            [evalucar.evalufall.miapp.eventos-efectos.usuario]
            [reagent-material-ui.core.grid :refer [grid]]
            [evalucar.evalufall.miapp.querys.navegacion]
            ["@material-ui/core" :refer [Box Button]]
            ))
(def menu-logeado [{:key "Calculo-01" 
                    :id :calculo-01
                    :xs 2
                    :component "a"
                    :href "#calculo-01"
                    :ml 2
                    :pb 2}
                   {:key "Calculo-02"
                    :id :calculo-02
                    :xs 2
                    :component "a"
                    :href "#calculo-02"
                    :ml 2
                    :pb 2}])


(defn tipo-choques []
  [grid {:container true :item true :xs 12 :sm 10}
   [grid  {:item true
           :xs 4}
    [:> Box {:key "Prime"
             :id :cupones
             :component "a"
             :href "#primero"
             } "un texto para Box 1" ]]
   [grid  {:item true
           :xs 4}
    [:> Box {:key "Primer"
             :id :cupones
             :component "a"
             :href "#primero"
             } "un texto para Box 2"]]
   (for [opcion  menu-logeado]
     [grid  {:item true
             :key (str "grid-" (:key  opcion)) ;; debe ser única
             :xs 4}
      [:> Box {:key (:key  opcion) ;; debe ser única
               :id (:id   opcion)
               :xs 2
               :component "a"
               :href (:href opcion)
               :ml 2
               :pb 2} (:key  opcion)]]  )
   ])


(defn tipo-choques-old [] ;; ¿que tipo de experimento era esto? no lo entiendo. ¿Por qué usar Box si Grid se bueno?
  [:> Box {:display "flex"
           :justify-content "flex-end"
           :py 1
           :px 1}
   [:> Box {:key "Primero" 
            :id :cupones
            :xs 2
            :component "a"
            :href "#primero"
            :ml 2
            :pb 2}]
   (for [opcion  menu-logeado]
     [:> Box {:key (:key  opcion)
              :id (:id   opcion)
              :xs 2
              :component "a"
              :href (:href opcion)
              :ml 2
              :pb 2}
      (if  true (:key  opcion)(:key  opcion) )])
   ])


(defn welcome-page
  []
  [:<> 
   #_(println "olkjpcion")
   #_[:> Box option "welcome-page"]
   [grid {:container true
          :item true
          :spacing 3
          :justify "space-between"}
    [grid {:item true :xs false :sm 1} ]
    [grid
     {:container true :item true :xs 12 :sm 10}
     "Bienvenido. Logeate para empezar. Solo tienes que tener una cuenta en gmail y pulsar en el enlace LOG-IN "]
    [grid {:item true :xs 0 :sm 1} ]
    ]
   ])


(defn main-page-temp
  []
  [:<> 
   [grid {:container true
          :item true
          :spacing 3
          :justify "space-between"}
    [grid {:item true :xs 0 :sm 1} ]
    [tipo-choques]
    [grid {:item true :xs 0 :sm 1} ]
    ]
   ]
  )

(defn main-page
  []
  [:<> 
   (for [opcion  menu-logeado]
     #_(println "olkjpcion")
     #_(println opcion)
     [:> Box opcion "main  --page"]
     #_[:> Box {:key (:key  opcion)
                :id (:id   opcion)
                :xs 2
                :component "a"
                :href (:href opcion)
                :ml 2
                :pb 2}
        (if  true (:key  opcion)(:key  opcion) )])
   ]
  )


(defn cuerpo-web []
  (let [page @(rf/subscribe [:query-active-page]) ]
    (case page
      :main [main-page-temp]
      :welcome [welcome-page]
      [welcome-page]
      )))
