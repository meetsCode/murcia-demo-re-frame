(ns evalucar.evalufall.miapp.componentes.datos-usuario
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ;;##### Ejemplo #####@@@@|||\\\
            [evalucar.evalufall.miapp.eventos-efectos.usuario]
            ))


(def usuario-prueba {:usuario {:displayName "Luis Sánchez Peña"
                               :uid "103530389907893518564"
                               :email "sanchezlsp@gmail.com"
                               :providerId "google.com"
                               :photoURL "https://lh3.googleusercontent.com/a-/AOh14Gjvhq5mcLsA4lTeryfYf2I-XXewyA_3zv5kAO7fgA=s96-c"
                            :auth {:accessToken nil }
                            :tipo [:usuario :gestor :administrador ]}
                  :errors {}
                  :nav {:active-page :welcome
                        :active-nav :welcome} 
                  :casos {:activo nil
                          :listado []}})


(def usuario-nil {:usuario {:displayName nil
                            :uid nil
                            :email nil
                            :providerId nil
                            :photoURL nil
                            :auth {:accessToken  nil}
                            :tipo []}
                  :errors {}
                  :nav {:active-page :welcome
                        :active-nav :welcome}
                  :casos {:activo nil
                          :listado []}})


(def usuario-html [:>
              [:p "mi nombre (nickname) es " ]
              [:p "mi uuid es: "]
              [:p "mi auth es: " ]
              [:p "mi tipos son: " ]
              [:p "mi error es: " ]
              [:p "mi :active-page es: " ]
              [:p "mi :active-nav es: " ]
              [:p "mi :caso-activo es: " ]
              [:p "mi :listado de casos es: " ]
              ])

(defn usuario []
  (let [usr (rf/subscribe [:query-usr])]
    (println "y el usuario es: " @usr)
    usuario-html))
