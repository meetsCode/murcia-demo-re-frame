(ns  firma.proyecto.miapp.fireb  ;;bxf.frontend.app el ns es el nombre completo del fichero
  ;; se quita el trozo del path definido en shadow-cljs.edn
  ;; en este caso es src/
  (:require [clojure.string :as str]
            ;; -- My App Imports -- ;;
            ;;##### Ejemplo #####@@@@|||\\\
						["firebase/app"  :refer [initializeApp]]
						["firebase/database"
             :refer [getDatabase ref set onValue  child get]
             :rename {ref reffb
                      set setfb
                      onValue on-value-fb
                      child child-fb
                      get get-fb}]
						;;["firebase/database"  :refer [getDatabase ] ]
           ))


(def  firebaseConfig {
                      :apiKey "AIzaSyBgswgXzhrt9tb3ax7KgSDtGr7uy4m91mo"
                      :authDomain "soportes3.firebaseapp.com"
                      :projectId "soportes3"
                      :storageBucket "soportes3.appspot.com"
                      :databaseURL "https://soportes3-default-rtdb.europe-west1.firebasedatabase.app"
                      :messagingSenderId "871772136450"
                      :appId "1:871772136450:web:4d6726422803893465731b"
                      :measurementId "G-G8JVKY3ZN6"
                      })


(defn firebase-init
  []
  ;;  #_(def mifirebasee (.initializeApp firebase (clj->js firebaseConfig)))
  (println "   -----------   -----------  -----------  -----------     "  )
  (.log js/console " ----------------- Ini de arranque Firebase ---------")
  (println (.toString (js/Date.)) " uuid ->" (uuid "3") )

  (def fire (initializeApp (clj->js firebaseConfig)  ))

  (.log js/console fire)
  (.log js/console " ----------------- Fin de arranque Firebase ---------")
  fire
  )


(defn fire-database-ini
  []
  (def fire-db (getDatabase)))


(defn muestra-database
  []
  (println " <---------->fireb/muestra-database:  Realtime Database " )
  (.log js/console fire ))



(defn escribe-datos
  []
  (println " <---------->fireb/escribe-datos:   " )
  (let [path "gigi"
        datos ( str "SPA-app  "
               (.getHours (js/Date.)) ":" (.getMinutes (js/Date.))
               " "
               (first (str/split (.toDateString (js/Date.)) #" "))
               " "(get  ["domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado" ] (.getDay (js/Date.)))
               ", " (.getDate (js/Date.))
               " de Sep "
               )
        db fire-db ;; (getDatabase)
        referen (reffb db path )]
    (setfb referen datos )
    (println "escribí" "SPA-app ....")
  #_(setfb referen (clj->js {:hola 5 :adios "fin"}))))


(defn muestra-fire-cloud
  [snaps] 
  (println "Aquí viene lo que hay en <<users>>  ")
  (.log js/console  " (.val snaps )" snaps ))


(defn muestra 
  [dato]
  (println " (.val " dato ")"))


(defn lee-on-firebase
  [path]
  (println "<----------->fireb/lee-on-firebase: ")
  (let [db (getDatabase)
        starCountRef (reffb db "gigi")
        nose ( atom "unstrin atom") ] 
  (on-value-fb
   starCountRef
   ;;(fn [dato] (println  (.exportVal (js->clj dato))))))
   ;;(fn [dato] (println (js->clj dato :keywordize-keys true) #_(js->clj  (.toJSON (js->clj dato)))))
   (fn [snapshot]
     ;;(.log js/console  (js->clj dato :keywordize-keys true))
     (.log js/console  (js->clj (.val snapshot) :keywordize-keys true))
     (println "ARIBA es (.val snapshot) ABAJO de (.toJSON snapshot)")
     (.log js/console  (js->clj (.toJSON snapshot) :keywordize-keys true))
     ;;(reset! nose (js->clj (. snapshot val) :keywordize-key true))
     ;;(js->clj dato :keywordize-keys true)
                    )
   )))





(defn lee-once-firebase
  []
  (println " Desde dentro de miapp.fireb/lee-once-firebase -> " )
  (let [db fire
        referencia (reffb db "gigi")
        funcion (fn [captura]  (println "---El dato vale:    " (str  captura ) )) ]
    (on-value-fb referencia funcion))
  )



(defn show-registro
  [path]
  (let [ruta "gigi"] 
    (.log js/console " <-- ref --> referencia de ruta ->  "
          ;;(reffb (getDatabase)  ruta))
          fire)
    (println " Desde dentro de miapp.fireb/lee-once-firebase ->  "
             (reffb fire )))
  )
