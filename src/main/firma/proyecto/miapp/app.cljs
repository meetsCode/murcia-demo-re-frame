(ns firma.proyecto.miapp.app
  ;;bxf.frontend.app el ns es el nombre completo del fichero
  ;; se quita el trozo del path definido en shadow-cljs.edn
  ;; en este caso es src/
  (:require [clojure.string :as str]
            [reagent.core :as r]
            [reagent.dom :as rdom]
            ;;[re-frame.core :as rf ]
            ;; -- My App Imports -- ;;
            ;;##### Ejemplo #####@@@@|||\\\
						;;[ firma.proyecto.miapp.firebcloud :as fire ]  ;; si es una lib de clojure
						[ firma.proyecto.miapp.fireb :as fire ]  ;; si es una lib de clojure
            ;;["loquesea" :as lib-a]  ;; si es una lib de JavaScript
            ))

(println "Eso es un print cuando se arranca la librería")


(defn appi
  []
  [:<>
   [:div
		[:hr]
		;;[:input "holita"]
		[:h1 "titutlo en H1"]
		[:h2 (str "hola desde h2"  #_(java.time.LocalDateTime/now) )]
		[:p (str "La fecha/hora actual es: " )]
		[:h3 (str (.toISOString (js/Date.)))]

    [:button {:onClick (fn [evnt]
                         (fire/firebase-init))}
     "Inicializacion Firebase"]
    [:button {:onClick (fn [evnt]
                         (fire/fire-database-ini)
                         )}
     "Crea base de datos"]
    [:button {:onClick (fn [evnt]
                         (fire/muestra-database ))}
     "Database"]

    #_[:p " Datos intermedios: "]
    #_[:button {:onClick (fn [evnt]
                         (fire/show-registro "gigi" ))}
     "Registo gig"]

    [:p " para la escritura hay una opción: "] 
    [:button {:onClick (fn [evnt] #_(fire/db-save! "ungigi" "locura")
                         (fire/escribe-datos ))}
     "Escribe datos"]

    [:p " para la lectura hay dos opciones: "] 
    [:button {:onClick (fn [evnt]
                         (fire/lee-on-firebase "datos"))}
     " lectura ON:"]

    #_[:button {:onClick (fn [evnt]
                         (fire/lee-once-firebase))}
     " lectura ONCE:"]
    [:p " Lo que escribas aquí se verá en console.log"] 
    [:input {:onChange (fn [evnt]
                         (.log js/console "evnt")
                         (println (-> evnt .-target .-value)))}]
    ]])


(defn ^{:dev/after-load true , :after-load true} start
  []
  (rdom/render [appi]
               (.getElementById js/document "app")))


(defn ^:export init []
  (println "hello world, desde Refarma con db inicializada")
  (println "Tienes todo el estado en la variable re_frame.db.app_db.state")
  ;;(rf/dispatch-sync [:initialize-db]) mejor en start? creo que no;; este evento de DataBase lo cree y cargué en el ns bxf.frontend.db (rf/reg-event-db....)
  (start))
