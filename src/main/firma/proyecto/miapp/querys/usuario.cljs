(ns  evalucar.evalufall.miapp.querys.usuario
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ;;##### Ejemplo #####@@@@|||\\\
            ))
;; Las query se registran con la función rf/reg-sub
;; imagino que viene de re-frame registra subscripción.
;; para mas información ver apuntes GoodNotes > Clojure y Web > pag52

(rf/reg-sub
 :query-usr-uid
 (fn [db query]
   (get-in db [:usuario :uid] nil )))


