(ns  evalucar.evalufall.miapp.querys.navegacion
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ;;##### Ejemplo #####@@@@|||\\\
            ))
;; Las query se registran con la función rf/reg-sub
;; imagino que viene de re-frame registra subscripción.
;; para mas información ver apuntes GoodNotes > Clojure y Web > pag52

(rf/reg-sub
 :query-active-nav
 (fn [db query]
   (get-in db [:nav  :active-nav] nil )))


(rf/reg-sub
 :query-active-page
 (fn [db query]
   (get-in db [:nav  :active-page] nil )))


