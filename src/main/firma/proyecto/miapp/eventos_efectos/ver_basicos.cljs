(ns  evalucar.evalufall.miapp.eventos-efectos.ver-basicos
  (:require [clojure.string :as str]
            [re-frame.core :as rf ]
            ;;##### Ejemplo #####@@@@|||\\\
            ["firebase/app" :as firebase]
            [ evalucar.evalufall.miapp.firebase.auth  :refer [ sign-in-with-google! sign-out-google! ]]
            ))
;; Los eventos (event) se registran con la función rf/reg-event-fx [cntxt llamada]
;; o con la función rf/reg-event-db [cntxt llamada] si es acceso directo a la base de datos.
;; Los efectos (fx) se registran con la función rf/reg-fx [ ]
;; para mas información ver apuntes GoodNotes > Clojure y Web > pag51

#_(rf/reg-event-db  ;; esto está comentado para que pueda usarlo en el futuro. Pero ahora no necesito cambiar :db
 :guardar-usuario 
 (fn [db query]
   (println "estoy dentro de :guardar usuario. Guardaré" query)
   (let [user-data (second query)]
     (if-not user-data
       (update-in db [:usuario] {:nickname nil
                                 :uid nil
                                 :auth nil
                                 :tipo []})
       (update-in db [:usuario] user-data))
     )
   ))


(rf/reg-event-fx
 :ver-db
 (fn [cntxt llamada]
   (println "alguien llamó al envento evalucar.evalufall.miapp.eventos-efectos.ver-basicos>:ver-db")
   {:console (:db cntxt)}))


(rf/reg-event-fx
 :ver-cntxt
 (fn [cntxt llamada]
   (println "alguien llamó al envento evalucar.evalufall.miapp.eventos-efectos.ver-basicos>:ver-cntxt")
   {:console cntxt}))


(rf/reg-fx
 :console
 (fn [ objeto ]
   (println "alguien llamó al efecto ver_basicos>:console ")
   (println objeto)
   ))
