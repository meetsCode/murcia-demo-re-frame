(ns  evalucar.evalufall.miapp.firebase.auth
  ;;bxf.frontend.app el ns es el nombre completo del fichero
  ;; se quita el trozo del path definido en shadow-cljs.edn
  ;; en este caso es src/
  (:require [clojure.string :as str]
            ;; -- My App Imports -- ;;
            ;;##### Ejemplo #####@@@@|||\\\
						["firebase/app"  :refer [initializeApp] :as firebase ]
            [re-frame.core :as rf ]
						#_["firebase/database"
             :refer [getDatabase ref set onValue  child get]
             :rename {ref reffb
                      set setfb
                      onValue on-value-fb
                      child child-fb
                      get get-fb}]
						["firebase/auth" :as auth ]
						;;["firebase/analytics"  ]
						;;["firebase/database"  :refer [getDatabase ] ]
            ))
(defn sign-in-with-google!
  []
  (let [ provider (auth/GoogleAuthProvider.)
        auten (auth/getAuth)]
    ;; (.signInWithPopup (firebase/auth) (firebase/auth.GoogleAuthProvider.)) ;; es la versión vieja de los vídeos.
    (auth/signInWithPopup auten provider)
    ) )


(defn sign-out-google!
  []
  (let [authen (auth/getAuth)]
    ;; (.signOut (firebase/auth) )  ;; es la versión vieja, la de los vídeos.
    (auth/signOut authen)))


(def  usuario-nil {:usuario {:displayName nil
                            :uid nil
                            :providerId nil
                            :email nil
                            :photoURL nil
                            :accessToken  nil
                            :tipo []}
                  :errors {}
                  :nav {:active-page :welcome
                        :active-nav :welcome}
                  :casos {:activo nil
                          :listado []}})


(defn auth-change-observer
  []
  (let [authen (auth/getAuth)]
    (auth/onAuthStateChanged authen
                             (fn [user]
                               (if user
                                 ;;(.log js/console user)
                                 (do
                                   (rf/dispatch [:change-page-web  :main ])
                                   (rf/dispatch [:change-nav-web  :main ])
                                   (rf/dispatch [:guardo-usuario-local user]))
                                 (do
                                   (rf/dispatch [:change-page-web :welcome])
                                   (rf/dispatch [:change-nav-web  :welcome])
                                   (rf/dispatch [:guardo-usuario-local nil])))
                               ))))
