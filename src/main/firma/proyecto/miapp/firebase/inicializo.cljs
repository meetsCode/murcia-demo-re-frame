(ns  evalucar.evalufall.miapp.firebase.inicializo
  (:require [clojure.string :as str]
            ;;##### Ejemplo #####@@@@|||\\\
						["firebase/app"  :refer [initializeApp] :as firebase]
						["firebase/analytics" :refer [getAnalytics] ]
            [evalucar.evalufall.miapp.firebase.auth :as auth]
           ))


(def  firebaseConfig {
                      :apiKey "AIzaSyCbsX2E0MUQgBiMnRBR-CxWGyZAvkm7IQ0"
                      :authDomain "cochechoca.firebaseapp.com"
                      :projectId "cochechoca"
                      :storageBucket "cochechoca.appspot.com"
                      ;;:databaseURL "https://soportes3-default-rtdb.europe-west1.firebasedatabase.app"
                      :messagingSenderId "672875323446"
                      :appId "1:672875323446:web:84767bec4809a4fbe97897"
                      :measurementId "G-3SCVNVFYN9"
                      })


(defn firebase-init
  []
  #_(if (zero? (alength firebase/apps))
    (println "no había inicializado")
    (println " había inicializado"))
  (println "   -----------   -----------  Ini de arranque Firebase ---------")
  (println (.toString (js/Date.)) " uuid ->" (uuid "3") )

  (def fire-app (initializeApp (clj->js firebaseConfig)))
  (def analytics (getAnalytics fire-app))
  (.log js/console " El objeto fireapp (fire-app) :")
  (.log js/console fire-app)
  (.log js/console analytics)
  ;;(println firebase/app)
  (auth/auth-change-observer) ;; importante para el login logout
  (.log js/console " ----------------- Fin de arranque Firebase ---------")
  ;;fire-app
  )
